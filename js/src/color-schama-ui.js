import Picker from "vanilla-picker/dist/vanilla-picker";
import axios from "axios";
import AWN from "awesome-notifications";
import color from "color";

(async ($, drupalSettings) => {
  Drupal.behaviors.addLocalTask = {
    attach: () => {
      if (!_.isEmpty(drupalSettings.color_schema_ui.current_route)) {
        $(".user-logged-in .nav-tabs.tabs--primary")
          .once()
          .append('<li><a href="#" title="Color schema UI toggle" class="color-schema-ui-toggle disabled">Color schema UI toggle</a></li>');
      }
    }
  };

  /**
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.colorSchemaUi = {
    attach: async () => {
      Drupal.behaviors.colorSchemaUi.initColorChanger();
    },
    initColorChanger() {
      $(".color-schema-ui-toggle").once('color-change-ui-init').on("click", event => {
        const toggleButton = $(event.currentTarget);
        // event.preventDefault();
        if (toggleButton.hasClass("disabled")) {
          toggleButton.removeClass("disabled").addClass("enabled");
          this.activateColorChanger();
        } else {
          toggleButton.removeClass("enabled").addClass("disabled");
          this.deactivateColorChanger();
        }
      });
    },
    async activateColorChanger() {
      $("body").append(drupalSettings.color_schema_ui.html_template);
      this.currentColors = await this.getInitialColors();

      // Set up notifications
      this.notifier = new AWN({
        position: "top-right",
        notify_callback: () => {
          window.location.reload();
        }
      });

      // Colorize buttons.
      $.each(this.currentColors, (key, value) => {
        this.updateColorButton(key, value);
      });

      // Inits picker per color
      this.initColorPickers(this.currentColors);
      // Init save action
      $("#color-schema-ui button.save").on("click", () => this.save());
    },
    deactivateColorChanger() {
      $("#color-schema-ui").remove();
    },
    async getInitialColors() {
      const url = `${drupalSettings.color_schema_ui.get_initial_colors}?${this.getRandomId()}`;
      return axios.post(url).then(resp => resp.data);
    },
    async updatePreviewCss(colorData) {
      const url = `${drupalSettings.color_schema_ui.get_compiled_scss}?${this.getRandomId()}`;
      // get_compiled_scss
      const inlineStyles = await axios.post(url, JSON.stringify(colorData)).then(resp => resp.data);
      // Preview is done by inline style.
      $("#colorSchemaUiInlineStyles").remove();
      $('<style id="colorSchemaUiInlineStyles">').text(inlineStyles).appendTo(document.head);
    },
    initColorPickers(colors) {
      $.each(colors, (key, value) => {
        new Picker({
          parent: $(`.${this.machineName(key)}`)[0],
          color: value,
          onChange: newVal => this.pickedColor(newVal, key),
          onDone: newVal => this.pickedColor(newVal, key, true)
        });
      });
    },
    async pickedColor(pickerValue, key, updatePreviewCss) {
      const button = $("#color-schema-ui button");
      const val = pickerValue.rgbaString;
      this.updateColorButton(key, val);
      if (updatePreviewCss) {
        button.prop("disabled", true);
        this.currentColors[this.machineName(key)] = val;
        await this.updatePreviewCss(this.currentColors);
        button.prop("disabled", false);
      }
    },
    async save() {
      const buttons = $("#color-schema-ui button:not(.picker_done)");
      buttons.prop("disabled", true);
      // compile_scss_to_filesystem
      const url = `${drupalSettings.color_schema_ui.compile_scss_to_filesystem}?${this.getRandomId()}`;
      await axios.post(url, JSON.stringify(this.currentColors)).then(resp => resp.data);
      this.notifier.success(
        Drupal.t(
          "Theme colors saved."
        )
      );
      buttons.prop("disabled", false);
      this.deactivateColorChanger();
    },
    updateColorButton(key, value) {
      $(`#color-schema-ui div.${this.machineName(key)} button`)
        .css("background-color", value)
        .css("color", color(value).luminosity() > 0.5 ? "#000" : "#fff");
    },
    machineName(name) {
      return name.replace(/-/g, "_");
    },
    getRandomId() {
      let text = "";
      const chars =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      for (let i = 0; i < 5; i++) {
        text += chars.charAt(Math.floor(Math.random() * chars.length));
      }
      return text;
    }
  };
})(window.jQuery, window.drupalSettings);
